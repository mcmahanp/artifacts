'''add artifacts to png images'''
import scipy as sp
import png


def png2array(filename):
    pr = png.Reader(filename=filename)
    l = [row for row in pr.read()[2]]
    bd = pr.bitdepth
    if bd<8:
        bd = 8
    return(sp.array(l,dtype='uint%d'%bd))


def array2png(a,filename,irng=None,obits=None):
    if irng==None:
        irng = a.max()
    ibits = int(sp.log2(irng+1))+1
    if obits==None:
        obits = ibits
    orng = 2**obits

    png.from_array(a*orng/irng,'L;%d'%obits).save(filename)


    





m = png2array('/Users/mcmahanp/Downloads/diacritical2.png')[:,2::3]