'''this is just experimenting, with the mario brothers
question-mark box'''

import scipy as sp
import png

# the data
Y = 5-sp.array([
    [5,2,2,2,2,2,2,2,2,2,2,2,2,2,2,5],
    [2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5],
    [2,0,5,0,0,0,0,0,0,0,0,0,0,5,0,5],
    [2,0,0,0,0,2,2,2,2,2,0,0,0,0,0,5],
    [2,0,0,0,2,2,5,5,5,2,2,0,0,0,0,5],
    [2,0,0,0,2,2,5,0,0,2,2,5,0,0,0,5],
    [2,0,0,0,2,2,5,0,0,2,2,5,0,0,0,5],
    [2,0,0,0,0,5,5,0,2,2,2,5,0,0,0,5],
    [2,0,0,0,0,0,0,2,2,5,5,5,0,0,0,5],
    [2,0,0,0,0,0,0,2,2,5,0,0,0,0,0,5],
    [2,0,0,0,0,0,0,0,5,5,0,0,0,0,0,5],
    [2,0,0,0,0,0,0,2,2,0,0,0,0,0,0,5],
    [2,0,0,0,0,0,0,2,2,5,0,0,0,0,0,5],
    [2,0,5,0,0,0,0,0,5,5,0,0,0,5,0,5],
    [2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5],
    [5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5],
],dtype='uint8')

subY = Y[3:14,4:12]

testY = sp.array([
 [52 , 55 , 61 , 66 , 70 , 61 , 64 , 73],
 [63 , 59 , 55 , 90 , 109, 85 , 69 , 72],
 [62 , 59 , 68 , 113, 144, 104, 66 , 73],
 [63 , 58 , 71 , 122, 154, 106, 70 , 69],
 [67 , 61 , 68 , 104, 126, 88 , 68 , 70],
 [79 , 65 , 60 , 70 , 77 , 68 , 58 , 75],
 [85 , 71 , 64 , 59 , 55 , 61 , 65 , 83],
 [87 , 79 , 69 , 68 , 65 , 76 , 78 , 94]
])

def writePng(x,fn,irng,obits):
    orng=2**obits-1
    png.from_array(x*orng/irng,'L;%d'%obits).save(fn)




# DCT

def dctPixel(x,u,v):
    if u==0:
        alphau = sp.sqrt(1./x.shape[0])
    else:
        alphau = sp.sqrt(2./x.shape[0])
    if v==0:
        alphav = sp.sqrt(1./x.shape[1])
    else:
        alphav = sp.sqrt(2./x.shape[1])
    rows = sp.arange(x.shape[0]).reshape((x.shape[0],1))
    cols = sp.arange(x.shape[1]).reshape((1,x.shape[1]))
    return((alphau*alphav*x*sp.cos((rows+0.5)*sp.pi*u/x.shape[0])*sp.cos((cols+0.5)*sp.pi*v/x.shape[1])).sum())
def dct(x,irng,orng=255):
    res = x.astype(dtype='int64').copy()
    # scale and center
    res *= orng
    res /= irng
    res -= orng/2+1
    # transform
    out = sp.zeros(res.shape,dtype='float64')
    for u in xrange(res.shape[0]):
        for v in xrange(res.shape[1]):
            out[u,v] = dctPixel(res,u,v)
    return(out)

def invdctPixel(x,i,j):
    alpha = sp.ones(x.shape)*sp.sqrt(2./x.shape[0])*sp.sqrt(2./x.shape[1])
    alpha[0,:] /= sp.sqrt(2)
    alpha[:,0] /= sp.sqrt(2)
    rows = sp.arange(x.shape[0]).reshape((x.shape[0],1))
    cols = sp.arange(x.shape[1]).reshape((1,x.shape[1]))
    return((alpha*x*sp.cos((i+0.5+sp.pi*rows/x.shape[0]))*sp.cos((j+0.5)*sp.pi*cols/x.shape[1])).sum())
def invdctPixel(x,i,j):
    alpha = sp.ones(x.shape)*sp.sqrt(2./x.shape[0])*sp.sqrt(2./x.shape[1])
    alpha[0,:] /= sp.sqrt(2)
    alpha[:,0] /= sp.sqrt(2)
    res = 0
    for u in xrange(x.shape[0]):
        for v in xrange(x.shape[1]):
            res += alpha[u,v]*x[u,v]*sp.cos(sp.pi*(i+0.5)*u/x.shape[0])*sp.cos(sp.pi*(j+0.5)*v/x.shape[1])
    return(res)
def invdct(x,orng):
    out = sp.zeros(x.shape,dtype='float64')
    for i in xrange(out.shape[0]):
        for j in xrange(out.shape[1]):
            out[i,j] += invdctPixel(x,i,j)
    out = out.round().astype('int64')
    out += 128
    out[out>255] = 255
    out[out<0] = 0
    #rescale
    out *= orng
    out /= 255
    return(out)





# quantization
defaultQ = sp.array([
    [16 , 11 , 10 , 16 , 24  , 40  , 51  , 61 ],
    [12 , 12 , 14 , 19 , 26  , 58  , 60  , 55 ],
    [14 , 13 , 16 , 24 , 40  , 57  , 69  , 56 ],
    [14 , 17 , 22 , 29 , 51  , 87  , 80  , 62 ],
    [18 , 22 , 37 , 56 , 68  , 109 , 103 , 77 ],
    [24 , 35 , 55 , 64 , 81  , 104 , 113 , 92 ],
    [49 , 64 , 78 , 87 , 103 , 121 , 120 , 101],
    [72 , 92 , 95 , 98 , 112 , 100 , 103 , 99 ]
])

def resizeQ(q,shape):
    res = q.copy()
    if shape[0] < q.shape[0]:
        res = res[:shape[0],:]
    elif shape[0] > q.shape[0]:
        res = sp.vstack((res,)+tuple([res[-1,:]]*(shape[0]-q.shape[0])))
    if shape[1] < q.shape[1]:
        res = res[:,:shape[0]]
    elif shape[1] > q.shape[1]:
        res = sp.hstack((res,)+tuple([res[:,-1].reshape((res.shape[0],1))]*(shape[1]-q.shape[1])))
    return(res)

def quantize(x,q=defaultQ):
    res = (x/resizeQ(q,x.shape)).round()
    return(res)

def dequantize(x,q=defaultQ):
    res = (x*resizeQ(q,x.shape))
    return(res)



def testPng(y,irng,orng,q=defaultQ):
    ibits = int(sp.log2(irng+1))+1
    obits = int(sp.log2(orng+1))+1
    writePng(y,'in.png',irng=irng,obits=ibits)
    writePng(invdct(dequantize(quantize(dct(y,irng),q=q),q=q),orng),'out.png',irng=orng,obits=obits)



