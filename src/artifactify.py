'''functions to add DCT artifacts to a scipy data array'''

import scipy as sp
from qmats import qmats



# Discrete cosine transform functions
# (these are very far from efficient)
def dctPixel(x,u,v):
    '''calculate dct for a pixel, given the rest of the matrix'''
    if u==0:
        alphau = sp.sqrt(1./x.shape[0])
    else:
        alphau = sp.sqrt(2./x.shape[0])
    if v==0:
        alphav = sp.sqrt(1./x.shape[1])
    else:
        alphav = sp.sqrt(2./x.shape[1])
    rows = sp.arange(x.shape[0]).reshape((x.shape[0],1))
    cols = sp.arange(x.shape[1]).reshape((1,x.shape[1]))
    return((alphau*alphav*x*sp.cos((rows+0.5)*sp.pi*u/x.shape[0])*sp.cos((cols+0.5)*sp.pi*v/x.shape[1])).sum())
def dct(x,irng):
    '''calculate the dct for a matrix, given the max value
    of that matrix (irng)'''
    res = x.astype(dtype='int64').copy()
    # scale and center
    res *= 255
    res /= irng
    res -= 128
    # transform
    out = sp.zeros(res.shape,dtype='float64')
    for u in xrange(res.shape[0]):
        for v in xrange(res.shape[1]):
            out[u,v] = dctPixel(res,u,v)
    return(out)

def invdctPixel(x,i,j):
    '''calculate inverse dct for a pixel, given the rest of the matrix'''
    alpha = sp.ones(x.shape)*sp.sqrt(2./x.shape[0])*sp.sqrt(2./x.shape[1])
    alpha[0,:] /= sp.sqrt(2)
    alpha[:,0] /= sp.sqrt(2)
    res = 0
    for u in xrange(x.shape[0]):
        for v in xrange(x.shape[1]):
            res += alpha[u,v]*x[u,v]*sp.cos(sp.pi*(i+0.5)*u/x.shape[0])*sp.cos(sp.pi*(j+0.5)*v/x.shape[1])
    return(res)
def invdct(x,orng):
    '''calculate the inverse dct for a matrix, given the max value
    of the output matrix (orng)''' 
    out = sp.zeros(x.shape,dtype='float64')
    for i in xrange(out.shape[0]):
        for j in xrange(out.shape[1]):
            out[i,j] += invdctPixel(x,i,j)
    out = out.round().astype('int64')
    out += 128
    out[out>255] = 255
    out[out<0] = 0
    #rescale
    out *= orng
    out /= 255
    return(out)

# quantization functions
def resizeQ(q,shape):
    res = q.copy()
    if shape[0] < q.shape[0]:
        res = res[:shape[0],:]
    elif shape[0] > q.shape[0]:
        res = sp.vstack((res,)+tuple([res[-1,:]]*(shape[0]-q.shape[0])))
    if shape[1] < q.shape[1]:
        res = res[:,:shape[0]]
    elif shape[1] > q.shape[1]:
        res = sp.hstack((res,)+tuple([res[:,-1].reshape((res.shape[0],1))]*(shape[1]-q.shape[1])))
    return(res)

def quantize(x,q):
    res = (x/resizeQ(q,x.shape)).round()
    return(res)

def dequantize(x,q):
    res = (x*resizeQ(q,x.shape))
    return(res)



# "public" functions
def artifactify(mat,inrange=None,outrange=None,quality=4):
    '''mat: the scipy array to add artifacts to
    inrange: a tuple of the min and max allowed values in mat, or None.
    outrange: a corresponding tuple of the min and max allowed values in the output, or None.
    quality: the finness of the quantization matrix, an integer in [0,8]'''
    if inrange==None:
        inrange = (mat.min(),mat.max())
    if outrange==None:
        outrange=inrange

    irng = inrange[1]-inrange[0]
    orng = outrange[1]-outrange[0]

    q = sp.array(qmats[quality])

    res = invdct(dequantize(quantize(dct(mat-inrange[0],irng),q=q),q=q),orng)
    return(res+outrange[0])




